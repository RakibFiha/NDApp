### NDApp Documentation ###

## What is and what is not NDApp ##

# What is NDApp designed to do #

NDApp (Node Deployment App) is decentralised peer to peer python socket app to help users setup proof of stake nodes.

The current test scope of NDApp limits to centralisation, with enough scope of growth potential NDApp is designed to decentralise the consensus layer of crypto.

NDApp communicates to the remote hardware from local machine of the users, peers validate the commands and the node deployment commands for chosen blockchain is sent to the node directly. This solves the problem for those who do not know how to set up node by themselves and also it redistributes the verification layer of the blockchain.

NDApp will have other main features such as saving log files and giving users full control over their privacy. Other main features include tracking the node and setting up the nodes and showing it to user's dashboard.

Secondary functionality includes node profitability calculator and hardware wallet compatibility.
